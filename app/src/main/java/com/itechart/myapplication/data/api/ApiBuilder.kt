package com.itechart.myapplication.data.api

import retrofit2.Retrofit


class ApiBuilder(private val retrofit: Retrofit) {

    fun buildStockApi(): StockApi = retrofit.create(StockApi::class.java)

}