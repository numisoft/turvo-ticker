package com.itechart.myapplication.data.api

import com.itechart.myapplication.data.entity.Stock
import com.itechart.myapplication.data.entity.StockList
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path


interface StockApi {

    @GET("real-time-price/{searchString}")
    fun getStock(@Path("searchString") searchString: String): Single<Stock>

    @GET("real-time-price/{searchString}")
    fun getStockList(@Path("searchString") searchString: String): Single<StockList>

}