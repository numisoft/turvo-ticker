package com.itechart.myapplication.data.repository

import com.itechart.myapplication.data.api.StockApi
import com.itechart.myapplication.data.entity.Stock
import com.itechart.myapplication.data.entity.StockList
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers


class Repository(private val stockApi: StockApi) {

    fun getData(searchString: String): Single<List<Stock>> = if (searchString.split(",").size > 1) {
        getStockList(searchString)
            .map { stockList -> stockList.stocks }
    } else {
        getStock(searchString)
            .map { stock -> listOf(stock) }
    }

    private fun getStockList(searchString: String): Single<StockList> = stockApi.getStockList(searchString)
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())

    private fun getStock(searchString: String): Single<Stock> = stockApi.getStock(searchString)
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())

}