package com.itechart.myapplication.data.entity

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize


@Parcelize
data class Stock(
    @SerializedName("symbol") val symbol: String,
    @SerializedName("price") val price: Double
) : Parcelable