package com.itechart.myapplication.data.entity

import com.google.gson.annotations.SerializedName


data class StockList(
    @SerializedName("companiesPriceList") val stocks: List<Stock>
)