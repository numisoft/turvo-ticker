package com.itechart.myapplication.ui.search

import android.os.Bundle
import android.view.View
import androidx.core.widget.addTextChangedListener
import com.itechart.myapplication.R
import com.itechart.myapplication.ui.main.RouterFragment
import kotlinx.android.synthetic.main.fragment_search.*


class SearchFragment : RouterFragment(R.layout.fragment_search) {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        searchButton.setOnClickListener { router?.goToListFragment(searchEditText.text.toString()) }
        searchEditText.addTextChangedListener { searchButton.isEnabled = it.toString().isNotEmpty() }
    }

    companion object {
        fun newInstance() = SearchFragment()
    }

}