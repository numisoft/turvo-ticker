package com.itechart.myapplication.ui.list.recycler

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.itechart.myapplication.R
import com.itechart.myapplication.data.entity.Stock


class StockAdapter : RecyclerView.Adapter<StockViewHolder>() {

    var stockList = emptyList<Stock>()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    var listener: ((Stock) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StockViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_stock, parent, false)
        return StockViewHolder(view).apply {
            itemView.setOnClickListener { listener?.invoke(stockList[adapterPosition]) }
        }
    }

    override fun onBindViewHolder(holder: StockViewHolder, position: Int) = holder.bind(stockList[position])

    override fun getItemCount(): Int = stockList.size

}