package com.itechart.myapplication.ui.list

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.itechart.myapplication.data.entity.Stock
import com.itechart.myapplication.data.repository.Repository
import io.reactivex.disposables.Disposable
import java.util.concurrent.TimeUnit


class ListViewModel(repository: Repository, searchString: String) : ViewModel() {

    val stocksLiveData: MutableLiveData<List<Stock>> = MutableLiveData(emptyList())
    val progressLiveData: MutableLiveData<Boolean> = MutableLiveData(false)
    val emptyViewLiveData: MutableLiveData<Boolean> = MutableLiveData(false)

    private var getStockDisposable: Disposable? = null

    init {
        getStockDisposable = repository.getData(searchString)
            .doOnSubscribe {
                progressLiveData.value = true
                emptyViewLiveData.value = false
            }
            .delay(1, TimeUnit.SECONDS)
            .doFinally { progressLiveData.postValue(false) }
            .subscribe({ onSuccess(it) }, { it.printStackTrace() })
    }

    private fun onSuccess(stocks: List<Stock>) {
        if (stocks.first().symbol != null) {
            stocksLiveData.postValue(stocks)
        } else {
            emptyViewLiveData.postValue(true)
        }
        progressLiveData.postValue(false)
    }

    override fun onCleared() {
        super.onCleared()
        getStockDisposable?.dispose()
    }

}
