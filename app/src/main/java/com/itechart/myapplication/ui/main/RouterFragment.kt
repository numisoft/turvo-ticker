package com.itechart.myapplication.ui.main

import android.content.Context
import androidx.fragment.app.Fragment

open class RouterFragment(layoutRes: Int) : Fragment(layoutRes) {

    var router: Router? = null

    override fun onAttach(context: Context) {
        super.onAttach(context)
        (context as? Router)?.let { router = it }
    }

}