package com.itechart.myapplication.ui.main

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.itechart.myapplication.R
import com.itechart.myapplication.data.entity.Stock
import com.itechart.myapplication.ui.detail.DetailFragment
import com.itechart.myapplication.ui.list.ListFragment
import com.itechart.myapplication.ui.search.SearchFragment


class MainActivity : AppCompatActivity(R.layout.activity_main), Router {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.container, SearchFragment.newInstance())
                .commit()
        }
    }

    override fun goToListFragment(searchString: String) = supportFragmentManager.beginTransaction()
        .setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_right)
        .replace(R.id.container, ListFragment.newInstance(searchString))
        .addToBackStack(ListFragment.javaClass.name)
        .commit()

    override fun goToDetailFragment(stock: Stock) = supportFragmentManager.beginTransaction()
        .setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_right)
        .replace(R.id.container, DetailFragment.newInstance(stock))
        .addToBackStack(DetailFragment.javaClass.name)
        .commit()

}
