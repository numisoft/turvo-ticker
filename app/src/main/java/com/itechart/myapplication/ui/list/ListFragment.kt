package com.itechart.myapplication.ui.list

import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.itechart.myapplication.R
import com.itechart.myapplication.ext.setVisibility
import com.itechart.myapplication.ui.list.recycler.StockAdapter
import com.itechart.myapplication.ui.main.RouterFragment
import kotlinx.android.synthetic.main.fragment_list.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf


class ListFragment : RouterFragment(R.layout.fragment_list) {

    private val viewModel by viewModel<ListViewModel> { parametersOf(stocksString) }
    private val stocksString by lazy { arguments?.getString(SYMBOL_KEY) }
    private val stockAdapter = StockAdapter().apply {
        listener = { router?.goToDetailFragment(it) }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupRecyclerView()
        subscribeOnStocks()
        backButton.setOnClickListener { activity?.onBackPressed() }
    }

    private fun setupRecyclerView() = stockRecyclerView.apply {
        adapter = stockAdapter
        layoutManager = LinearLayoutManager(context)
    }

    private fun subscribeOnStocks() = with(viewModel) {
        stocksLiveData.observe(viewLifecycleOwner, Observer { stockAdapter.stockList = it })
        progressLiveData.observe(viewLifecycleOwner, Observer { progressBar.setVisibility(it) })
        emptyViewLiveData.observe(viewLifecycleOwner, Observer { emptyStateTextView.setVisibility(it) })
    }

    companion object {
        fun newInstance(symbol: String) = ListFragment().apply {
            arguments = bundleOf(SYMBOL_KEY to symbol)
        }
        private const val SYMBOL_KEY = "symbol"
    }

}
