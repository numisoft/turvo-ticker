package com.itechart.myapplication.ui.main

import com.itechart.myapplication.data.entity.Stock

interface Router {

    fun goToListFragment(searchString: String): Int

    fun goToDetailFragment(stock: Stock): Int

}