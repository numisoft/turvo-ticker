package com.itechart.myapplication.ui.detail

import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import com.itechart.myapplication.R
import com.itechart.myapplication.data.entity.Stock
import com.itechart.myapplication.ext.toDecimalString
import kotlinx.android.synthetic.main.fragment_detail.*


class DetailFragment : Fragment(R.layout.fragment_detail) {

    private val stock: Stock? by lazy { arguments?.getParcelable<Stock>(STOCK_KEY) }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        closeButton.setOnClickListener { activity?.onBackPressed() }
        symbolTextView.text = stock?.symbol
        priceTextView.text = stock?.price?.toDecimalString()
    }

    companion object {
        fun newInstance(stock: Stock) = DetailFragment().apply {
            arguments = bundleOf(STOCK_KEY to stock)
        }
        private const val STOCK_KEY = "stock"
    }

}