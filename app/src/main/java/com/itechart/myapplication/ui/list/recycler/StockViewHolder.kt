package com.itechart.myapplication.ui.list.recycler

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.itechart.myapplication.data.entity.Stock
import com.itechart.myapplication.ext.toDecimalString
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_stock.*


class StockViewHolder(override val containerView: View) : RecyclerView.ViewHolder(containerView), LayoutContainer {

    fun bind(stock: Stock) {
        symbolTextView.text = stock.symbol
        priceTextView.text = stock.price.toDecimalString()
    }

}