package com.itechart.myapplication.ext


fun Double.toDecimalString(): String = String.format("%.2f", this)