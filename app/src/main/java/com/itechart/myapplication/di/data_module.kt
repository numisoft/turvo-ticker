package com.itechart.myapplication.di

import com.google.gson.Gson
import com.itechart.myapplication.data.api.ApiBuilder
import com.itechart.myapplication.data.repository.Repository
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory


val data_module = module {

    single {
        Retrofit.Builder()
            .baseUrl("https://financialmodelingprep.com/api/v3/stock/")
            .addConverterFactory(GsonConverterFactory.create(Gson()))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()
    }

    single { ApiBuilder(get()).buildStockApi() }

    single { Repository(get()) }

}