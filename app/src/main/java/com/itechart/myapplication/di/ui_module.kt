package com.itechart.myapplication.di

import com.itechart.myapplication.ui.list.ListViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module


val ui_module = module {

    viewModel { (searchString: String) -> ListViewModel(get(), searchString) }

}