package com.itechart.myapplication

import android.app.Application
import com.itechart.myapplication.di.data_module
import com.itechart.myapplication.di.ui_module
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin


class TurvoTickerApp: Application() {

    override fun onCreate() {
        super.onCreate()
        initKoin()
    }

    private fun initKoin() {
        startKoin {
            printLogger()
            androidContext(this@TurvoTickerApp)
            modules(listOf(data_module, ui_module))
        }

    }

}